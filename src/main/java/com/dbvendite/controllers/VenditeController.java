package com.dbvendite.controllers;

import java.util.List;
import java.util.Set;

import com.dbvendite.dao.ProdottoDataDao;
import com.dbvendite.dao.VenditeDao;
import com.dbvendite.dao.ViewVenditaProdottiDao;
import com.dbvendite.domain.Vendite;
import com.dbvendite.views.ViewVenditaProdotti;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="/api/vendite", produces = "application/json")
public class VenditeController{

    @Autowired
    VenditeDao venditeDao;
    @Autowired
    ProdottoDataDao prodottoDataDao;
    @Autowired
    ViewVenditaProdottiDao viewVenditaProdottiDao;
    
    @GetMapping(value="")
    public List<Vendite> getMethodName() {
        return venditeDao.findAll();
    }

    @PostMapping(value = "/save")
    public Vendite saveVendita(@RequestBody Vendite vendita){
        return this.venditeDao.save(vendita);
    }
    @PutMapping(value = "/update")
    public Vendite updateVendita(@RequestBody Vendite vendita){
        return this.venditeDao.saveAndFlush(vendita);
    }
    @DeleteMapping(value="/{numeroscontrino}")
    public ResponseEntity deleteVendita(@PathVariable Long numeroscontrino)  
    {
        return venditeDao.findById(numeroscontrino).map(vendita ->{
            this.venditeDao.delete(vendita);
            return ResponseEntity.ok().build();
        } ).orElse(ResponseEntity.notFound().build());
    }
    
    @GetMapping(value="/VenditaProdotti1")
    public List<Object[]> getProdottoDataAll() {
        return prodottoDataDao.getProdottoDataAll();
    }
    @GetMapping(value="/prodottidiversi1")
    public List<Object[]> getProdottoDiversiAll() {
        return prodottoDataDao.getProdottoDataDiverse();
    }

    @GetMapping(value="/VenditaProdotti2")
    public List<Object[]> getViewVenditaProdotti() {
        return prodottoDataDao.getViewVenditaProdotti();
    }
    @GetMapping(value="/VenditaProdotti3")
    public List<ViewVenditaProdotti[]> getViewVenditaProdotti3() {
        return viewVenditaProdottiDao.getViewVenditaProdotti();
    }
}