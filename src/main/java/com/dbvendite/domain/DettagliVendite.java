package com.dbvendite.domain;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;
@Getter @Setter
@Entity
public class DettagliVendite{
    @Id
    Long id;
    Long numeroScontrino;
    int riga;
    String prodotto;
    BigDecimal importo;
    Long clienteId;
}