package com.dbvendite.domain;
import java.time.LocalDateTime;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
@Entity
public class Vendite{
    @Id
    Long numeroScontrino;
    LocalDateTime data;

    @OneToMany(mappedBy="numeroScontrino",cascade = CascadeType.ALL)
    @JsonIgnore
    Set<DettagliVendite> dettagliVendite;
}