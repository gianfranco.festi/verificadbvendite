package com.dbvendite.domain;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;


import lombok.Getter;
import lombok.Setter;
@Getter @Setter
@Entity(name="clienti")
public class Cliente{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long clienteId;
    String cognome;
    int age;

    @OneToMany(mappedBy="clienteId",cascade = CascadeType.ALL)
    @JsonIgnore
    Set<DettagliVendite> dettagliVendite;
}