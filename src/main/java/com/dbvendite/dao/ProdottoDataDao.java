package com.dbvendite.dao;
import java.util.List;
import com.dbvendite.domain.DettagliVendite;
import com.dbvendite.views.ViewVenditaProdotti;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
@Repository
public interface ProdottoDataDao extends JpaRepository<DettagliVendite,Long>{

@Query(value="select Prodotto, Data from Vendite V join Dettagli_Vendite D on V.Numero_scontrino =D.Numero_Scontrino",nativeQuery = true)
List<Object[]> getProdottoDataAll();

@Query(value="select * from (select Prodotto, Data from Vendite V join Dettagli_Vendite D on V.Numero_scontrino =D.Numero_Scontrino) as VD1 where not exists (select * from (select Prodotto, Data from Vendite V join Dettagli_Vendite D on V.Numero_scontrino =D.Numero_Scontrino) as VD2 where VD1.Prodotto=VD2.Prodotto and VD1.Data<>VD2.Data)",nativeQuery = true)
List<Object[]> getProdottoDataDiverse();

@Query(value="select * from ViewVenditaProdotti",nativeQuery = true)
List<Object[]> getViewVenditaProdotti();

@Query(value="select * from ViewVenditaProdotti",nativeQuery = true)
List<ViewVenditaProdotti[]> getViewVenditaProdotti3();
}