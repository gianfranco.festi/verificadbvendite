package com.dbvendite.dao;


import com.dbvendite.domain.Vendite;
import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;
@Repository
public interface VenditeDao extends JpaRepository<Vendite,Long>{
}