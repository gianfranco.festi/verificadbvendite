package com.dbvendite.dao;
import java.util.List;
import com.dbvendite.views.ViewVenditaProdotti;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
@Repository
public interface ViewVenditaProdottiDao extends JpaRepository<ViewVenditaProdotti,Long>{
    @Query(value="select * from ViewVenditaProdotti",nativeQuery = true)
    List<ViewVenditaProdotti[]> getViewVenditaProdotti();
}