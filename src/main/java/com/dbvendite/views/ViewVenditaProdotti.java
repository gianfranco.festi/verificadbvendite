package com.dbvendite.views;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.Id;
import lombok.Getter;
import lombok.Setter;
@Getter @Setter
@Entity
public class ViewVenditaProdotti{
    @Id
    int id;
    String prodotto;
    LocalDateTime data;
}