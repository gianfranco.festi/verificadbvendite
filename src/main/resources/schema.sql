drop database dbvendite;

create database dbvendite;

use dbvendite;
create table clienti(
    cliente_id  int,
    cognome varchar(100),
    age int,
    constraint pk_clienti_cliente_id primary key(cliente_id)
);

create table vendite(
    numero_scontrino int,
    data datetime,
    constraint pk_vendite_numero_scontrino primary key(numero_scontrino) 
);

create table dettagli_vendite(
    id int,
    numero_scontrino int not null,
    riga int,
    prodotto varchar(255),
    importo decimal(13,4),
    cliente_id int ,
    constraint pk_dettagli_vendite_id primary key (id),
    constraint fk_dettagli_vendite_numero_scontrino foreign key(numero_scontrino) references vendite(numero_scontrino),
    constraint fk_dettagli_vendite_cliente_id foreign key(cliente_id) references clienti(cliente_id)
);
/*
create view ViewVenditaProdotti as     select 1 as id,Prodotto, Data         from Vendite V join Dettagli_Vendite D on V.Numero_scontrino =D.Numero_Scontrino;

create view ViewVenditeConTotale as 
    select * from ViewVenditaProdotti as VD1 
    where not exists (select * from ViewVenditaProdotti as VD2 where VD1.Prodotto=VD2.Prodotto and VD1.Data<>VD2.Data);

create view VenditeConTotale as 
    select Numero_Scontrino, sum(Importo) As Totale from Dettagli_Vendite group by Numero_Scontrino;


/*

use dbvendite;
delimiter //
create trigger dbvendite.trDeleteDettagliVenditeDaVendite
	before delete
    on dbvendite.vendite for each row
    begin
      delete from dettagli_vendite where numero_scontrino=OLD.numero_scontrino;
    end; //
delimiter ;

use dbvendite;
delimiter //
create trigger dbvendite.trDeleteDettagliVenditeDaCliente 
  before delete
  on dbvendite.clienti for each row
  begin
    delete from dbvendite.dettagli_vendite where cliente_id=OLD.cliente_id;
  end; //
delimiter ;
*/